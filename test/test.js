const assert = require('assert');
const params_js = require('params-js');
const jwt = require('jsonwebtoken');
const fs = require("fs-extra");
const http = require('http');
const Token = require('../token');
const key = fs.readFileSync("./test/test.private.pem");
const os = require('os');
const url = require('url');

const dummy_token = {
    "access_token": jwt.sign(JSON.stringify({
        user_name: "my_user",
        exp: Date.now() + 5000
    }), key),
    "token_type": "bearer",
    "refresh_token": jwt.sign(JSON.stringify({
        user_name: "my_user",
        exp: Date.now() + 15000
    }), key),
    "expires_in": 43199,
    "scope": "test"
};

describe('Tokengee', async () => {
    let apigee_login_mock_server;
    const config = require('./test.params.json');
    before(async function () {
        apigee_login_mock_server = http.createServer((req, res) => {
            res.setHeader('Content-Type', 'application/json');
            res.write(JSON.stringify(dummy_token));
            res.end();
        });
        apigee_login_mock_server.listen(8000, async () => {
            const params = await params_js(config, async item => Promise.resolve());
            params.TOKENGEE_ROLE = "test-role";
            const tokengee = new Token(params);
            await tokengee.get();
            await tokengee.save();
            await tokengee.load();
            test_token = tokengee.token;
            assert.equal(test_token.scope, "test");
            assert.equal(tokengee.toString(), dummy_token.access_token);
        });
    });

    after(async function () {
        apigee_login_mock_server.close();
    });

    it("GET username/password", async function () {
        const params = await params_js(config, async item => Promise.resolve());
        delete params.TOKENGEE_MFA;
        const tokengee = new Token(params);
        await tokengee.get();
        const test_token = tokengee.token;
        assert.equal(test_token.scope, "test");
    });
    it("GET username/password/mfa", async function () {
        const params = await params_js(config, async item => Promise.resolve());
        const tokengee = new Token(params);
        await tokengee.get();
        const test_token = tokengee.info();
        assert.equal(test_token.scope, "test");
    });
    it("Refresh", async function () {
        const params = await params_js(config, async item => Promise.resolve());
        const tokengee = new Token(params);
        await tokengee.get();
        await tokengee.refresh();
        const test_token = tokengee.info();
        assert.equal(test_token.scope, "test");
    });
    it("list", async function () {
        const params = await params_js(config, async item => Promise.resolve());
        params.TOKENGEE_DATA_DIR = ".test/list";
        params.TOKENGEE_ROLE = "a";
        const tokengee = new Token(params);
        try {
            await tokengee.save();
        } catch (err) {
            assert.equal(err.message, 'token not loaded');
        }
        await tokengee.get();
        await tokengee.save();
        const list = await tokengee.list();
        assert.deepEqual(list, ["a"]);
        const location = await tokengee.location();
        const location_url = url.pathToFileURL(location);
        const file_url = url.pathToFileURL(os.homedir() + "/" + params.TOKENGEE_DATA_DIR + "/a.token.json");
        assert.equal(url.fileURLToPath(location_url), url.fileURLToPath(file_url));
    });
    it("delete", async function () {
        const params = await params_js(config, async item => Promise.resolve());
        params.TOKENGEE_DATA_DIR = ".test/delete";
        params.TOKENGEE_ROLE = "b";
        const tokengee = new Token(params);
        await tokengee.get();
        await tokengee.save();
        const list = await tokengee.list();
        assert.equal(list[0], "b");
        await tokengee.delete();
        const empty_list = await tokengee.list();
        assert.deepEqual(empty_list, []);
    });
    it("token validity checks", async function () {
        const params = await params_js(config, async item => Promise.resolve());
        const tokengee = new Token(params);
        await tokengee.get();
        const token = tokengee.info();

        assert.equal(
            tokengee._is_token_valid(token.access_token, token.access_token_decoded.exp * 1000),
            false
        );
        assert.equal(
            tokengee._is_token_valid(token.access_token, token.access_token_decoded.exp * 1000 - 1),
            true
        );
        assert.equal(
            tokengee._is_token_valid(token.access_token, (token.access_token_decoded.exp * 1000) + 1),
            false
        );
        assert.equal(
            tokengee.is_access_token_valid(token.access_token_decoded.exp * 1000),
            false
        );
        assert.equal(
            tokengee.is_access_token_valid(token.access_token_decoded.exp * 1000 - 1),
            true
        );
        assert.equal(
            tokengee.is_access_token_valid((token.access_token_decoded.exp * 1000) + 1),
            false
        );
        assert.equal(
            tokengee._is_token_valid(token.refresh_token, token.refresh_token_decoded.exp * 1000),
            false
        );
        assert.equal(
            tokengee._is_token_valid(token.refresh_token, token.refresh_token_decoded.exp * 1000 - 1),
            true
        );
        assert.equal(
            tokengee._is_token_valid(token.refresh_token, token.refresh_token_decoded.exp * 1000 + 1),
            false
        );
        assert.equal(
            tokengee.is_refresh_token_valid(token.refresh_token_decoded.exp * 1000),
            false
        );
        assert.equal(
            tokengee.is_refresh_token_valid(token.refresh_token_decoded.exp * 1000 - 1),
            true
        );
        assert.equal(
            tokengee.is_refresh_token_valid(token.refresh_token_decoded.exp * 1000 + 1),
            false
        );
        assert.equal(
            tokengee.is_refresh_needed(token.access_token_decoded.exp * 1000 - 1000),
            false
        );
        assert.equal(
            tokengee.is_refresh_needed(token.access_token_decoded.exp * 1000 + 5000),
            true
        );
        assert.equal(
            tokengee.is_refresh_needed(token.access_token_decoded.exp * 1000),
            true
        );
    });
});