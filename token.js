const axios = require('axios');
const fs = require('fs-extra');
const jwt = require('jsonwebtoken');
const url = require('url');
const os = require('os');

module.exports = class Token {
    constructor(params) {
        this.params = {
            ...params
        };
        this.role = params.TOKENGEE_ROLE;
        this.dir = `${os.homedir()}/${params.TOKENGEE_DATA_DIR}`;
        this.apigee = axios.create(this.params.TOKENGEE_AUTH_ENDPOINT);
        this.url = new URL(`file://${this.dir}/${params.TOKENGEE_ROLE}.token.json`);
    }

    async load() {
        this.token = await fs.readJson(url.fileURLToPath(this.url));
    }

    async delete() {
        await fs.remove(url.fileURLToPath(this.url));
        this.token = null;
    }

    async save() {
        if (!this.token) throw new Error ("token not loaded");
        await fs.ensureFile(url.fileURLToPath(this.url));
        await fs.outputJSON(url.fileURLToPath(this.url), this.token, {
            spaces: "\t"
        });
    }

    async list() {
        const files = await fs.promises.readdir(this.dir);
        return files.map(v => v.toString().replace('\.token\.json', ''));
    }

    async refresh() {
        const res = await this._get_refresh();
        this.token = res.data;
    }

    async get() {
        const res = this.params.TOKENGEE_MFA ? await this._get_mfa() : await this._get_pswd();
        this.token = res.data;
    }

    async _get_pswd() {
        return await this.apigee.get("/", {
            params: {
                username: this.params.TOKENGEE_USER,
                password: this.params.TOKENGEE_PASSWORD,
                grant_type: "password"
            }
        });
    }
    async _get_mfa() {
        return await this.apigee.get("/", {
            params: {
                username: this.params.TOKENGEE_USER,
                password: this.params.TOKENGEE_PASSWORD,
                mfa_token: this.params.TOKENGEE_MFA,
                grant_type: "password"
            }
        });
    }

    async _get_refresh() {
        return await this.apigee.get("/", {
            params: {
                refresh_token: this.token.refresh_token,
                grant_type: "refresh_token"
            }
        });
    }

    is_access_token_valid(timestamp) {
        return this._is_token_valid(this.token.access_token, timestamp || Date.now());
    }
    is_refresh_token_valid(timestamp) {
        return this._is_token_valid(this.token.refresh_token, timestamp || Date.now());
    }
    is_refresh_needed(timestamp) {
        return !this.is_access_token_valid(timestamp) && this.is_refresh_token_valid(timestamp);
    }
    _is_token_valid(token, time_ms) {
        const token_data = jwt.decode(token);
        return token_data.exp * 1000 > time_ms;
    }

    info() {
        return {
            ...this.token,
            role: this.role,
            access_token_decoded: jwt.decode(this.token.access_token),
            refresh_token_decoded: jwt.decode(this.token.refresh_token)
        };
    }
    toString() {
        return this.token.access_token;
    }
    location() {
        return url.fileURLToPath(this.url);
    }
};