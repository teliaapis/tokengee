# Tokengee
Get and store Apigee tokens from login.apigee.com.
## Install
```bash
npm i -g tokengee
tokengee help
```
## Example
For [apigee-edge-maven-plugin](https://github.com/apigee/apigee-deploy-maven-plugin) users:
```bash
tokengee get -u my.user@mycompany.com
mvn install \
-Dapigee.authtype=oauth \
-Dapigee.username=my.user@mycompany.com \
-Dapigee.bearer=$(tokengee)
```