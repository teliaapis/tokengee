#!/usr/bin/env node

const params_js = require('params-js');
const config = require('./conf/tokengee.params.json');
const prompts = require('prompts');
const fs = require('fs-extra');
const Token = require("./token");

// Check if old node version is used
const version = process.version.replace("v", "").split(".").map((v) => parseInt(v));
if (version[0] * 100 + version[1] < 1012) {
    console.error(`Error: Engine version is ${process.version} and the minimum requirement is v10.12.0`);
    process.exit(1);
}

(async function main() {
    try {

        let params = await params_js(config, async item => Promise.resolve());

        //------- HELP -----------------

        if (params.TOKENGEE_HELP) {
            fs.createReadStream(__dirname + "/help.txt").pipe(process.stdout);
            return;
        }

        //------- GET -----------------

        if (params.TOKENGEE_GET) {
            params = await params_js(config, async item => {
                const response = await prompts({
                    type: item.secret ? 'password' : 'text',
                    name: 'val',
                    initial: item.default,
                    message: item.prompt
                });
                return response.val;
            });
            const token = new Token(params);
            await token.get();
            await token.save();
            log_success(token);
            log_token_info(token);
            return;
        }

        //------- REFRESH -----------------

        if (params.TOKENGEE_REFRESH) {
            const token = new Token(params);
            await token.load();
            if (!token.is_refresh_token_valid()) throw {
                message: "Token expired, please get a new token"
            };
            await token.refresh();
            await token.save();
            log_success(token);
            log_token_info(token);
            return;
        }

        //------- DELETE -----------------

        if (params.TOKENGEE_DELETE) {
            const token = new Token(params);
            await token.delete();
            return;
        }

        //------- LIST -----------------

        if (params.TOKENGEE_LIST) {
            const token = new Token(params);
            console.log();
            for (const file of await token.list()) {
                console.log(`- ${file}`);
            }
            return;
        }

        //------- INFO -----------------

        if (params.TOKENGEE_INFO) {
            try {
                const token = new Token(params);
                await token.load();
                log_token_info(token);
            } catch (err) {
                if (err.code === 'ENOENT') console.error("Error: Token does not exist, please obtain it with 'tokengee get'");
                else throw err;
            }
            return;
        }

        //------- TOKEN -----------------

        {
            const token = new Token(params);
            try {
                await token.load();
            } catch (err) {
                throw {
                    message: "Error: failed to load token, make sure you have got it with 'tokengee get'"
                };
            }
            if (!token.is_refresh_token_valid()) throw {
                message: "Token expired, please get a new token"
            };
            if (token.is_refresh_needed()) {
                await token.refresh();
                await token.save();
            }
            console.log(token.toString());
            return;
        }

    } catch (err) {
        console.error((err.response && err.response.data) || err.message);
    }

})();

function log_success(token) {
    console.log();
    console.log(`Success: authorization tokens saved to ${token.location()}`);
}

function log_token_info(token) {
    const token_info = token.info();
    console.log();
    console.log(`Role: ${token_info.role}`);
    console.log(`Authorized for: ${token_info.access_token_decoded.user_name}`);
    console.log(`Access token: ${data_to_string(token_info.access_token_decoded.exp*1000)}`);
    console.log(`Refresh token: ${(data_to_string(token_info.refresh_token_decoded.exp*1000))}`);
    console.log();

    function data_to_string(exp) {
        const diff = exp - Date.now();
        if (diff > 0) {
            let str = "valid till ";
            str += `${(new Date(exp)).toLocaleString()} `;
            str += `(${Math.trunc((diff)/3600000)} hrs)`;
            return str;
        } else {
            return "expired";
        }
    }
}